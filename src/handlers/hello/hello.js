import { successResponse } from '@/utils/lambda-response/lambda-response';

const hello = (event, context, callback) => {
  const response = successResponse({
    message: process.env.HELLO_MESSAGE,
  });
  callback(null, response);
};

export default hello;
