// According to the API Gateway specs, the body content must be stringified
export const stringify = json => JSON.stringify(json);

export const prepareBody = body => stringify(body);

export const lambdaResponse = statusCode => body => ({
  body: prepareBody(body),
  statusCode,
});

export const successResponse = lambdaResponse(200);
export const errorResponse = lambdaResponse(500);

export default {
  stringify,
  lambdaResponse,
  prepareBody,
  successResponse,
  errorResponse,
};
