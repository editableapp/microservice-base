import { stringify, lambdaResponse } from './lambda-response';

describe('lambdaResponse', () => {
  it('returns a well formed request', () => {
    const res = lambdaResponse(200)({ sample: true });
    expect(res).toEqual({
      statusCode: 200,
      body: JSON.stringify({ sample: true }),
    });
  });
});

describe('Stringify', () => {
  it('returns a stringified object version', () => {
    const obj = { yes: true, no: false, ok: 'ok' };
    const res = stringify(obj);
    const rollback = JSON.parse(res);
    expect(rollback).toEqual(obj);
  });
});
