module.exports = {
  "roots": [
    "<rootDir>/src"
  ],
	"testRegex": "(.*\\.test\\.(tsx?|jsx?))$",
  "transform": {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.jsx?$": "babel-jest"
  },
  "resolver": "jest-webpack-resolver",
  "jestWebpackResolver": {
    "silent": true,
    "webpackConfig": "./webpack.config.js"
  },
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
}
