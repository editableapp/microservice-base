![logo](./logo.png)

Modern Serverless Starter Kit forked from [postlight amazing work](https://github.com/postlight/serverless-babel-starter).

This kit is built on top top of the Serverless framework, giving you the latest in modern JavaScript (ES6 via Webpack + Babel, testing with Jest, linting with ESLint, and formatting with Prettier, aliases), the ease and power of Serverless, and a few handy helpers.

Once installed, you can create and deploy functions with the latest ES6 features in minutes, with linting and formatting baked in.

Note: Currently, this starter kit specifically targets AWS.

## Install

```bash
# If you don't already have the serverless cli installed, do that
npm install -g serverless

# Use the serverless cli to install this repo
serverless install --url https://gitlab.com/editableapp/microservice-base --name <your-service-name>

# cd into project and set it up
cd <your-service-name>

# Install dependencies
npm install
```

## Development

Creating and deploying a new function takes two steps, which you can see in action with this repo's default Hello World function.

#### 1. Add your function to `serverless.yml`

In the functions section of [`./serverless.yml`](./serverless.yml), you have to add your new function like so:

```yaml
functions:
  hello:
    handler: src/handlers/hello/hello.default
    events:
      - http:
          path: hello
          method: get
```

You can see here that we're setting up a function named `hello` with a handler at `src/handlers/hello/hello.js` (the `.default` piece is just indicating that the function to run will be the default export from that file). The `http` event says that this function will run when an http event is triggered (on AWS, this happens via API Gateway).

#### 2. Create your function

This starter kit's Hello World function (which you will of course get rid of) can be found at [`./src/handlers/hello/hello.js`](./src/handlers/hello/hello.js).
There you can see a basic function that's intended to work in conjunction with API Gateway (i.e., it is web-accessible). Like most Serverless functions, the `hello` function accepts an event, context, and callback. When your function is completed, you execute the callback with your response. (This is all basic Serverless; if you've never used it, be sure to read through [their docs](https://serverless.com/framework/docs/).

---

You can develop and test your lambda functions locally in a few different ways.

### Live-reloading functions

To run the hello function with the event data defined in [`fixtures/event.json`](fixtures/event.json) (with live reloading), run:

```bash
npm run watch:hello
```

### API Gateway-like local dev server

To spin up a local dev server that will more closely match the API Gateway endpoint/experience:

```bash
npm run serve
```

### Test your functions with Jest

Jest is installed as the testrunner. To create a test, co-locate your test with the file it's testing
as `<filename>.test.js` and then run/watch tests with:

```bash
npm run test
```

### Adding new functions/files to Webpack

When you add a new function to your serverless config, you don't need to also add it as a new entry
for Webpack. The `serverless-webpack` plugin allows us to follow a simple convention in our `serverless.yml`
file which is uses to automatically resolve your function handlers to the appropriate file:

```yaml
functions:
  hello:
    handler: src/hello.default
```

As you can see, the path to the file with the function has to explicitly say where the handler
file is. (If your function weren't the default export of that file, you'd do something like:
`src/hello.namedExport` instead.)


## Deploy

Assuming you've already set up your default AWS credentials (or have set a different AWS profile via [the profile field](serverless.yml#L25)):

```bash
npm run deploy
```

`npm run deploy` will deploy to "dev" environment. You can deploy to `stage` or `production`
with:

```bash
npm run deploy:stage

# -- or --

npm run deploy:production
```

After you've deployed, the output of the deploy script will give you the API endpoint
for your deployed function(s), so you should be able to test the deployed API via that URL.
